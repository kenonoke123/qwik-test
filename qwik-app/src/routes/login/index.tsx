import { component$ } from '@builder.io/qwik';
import { MUIButton, MuiForm, MuiInput, MuiInputLabel } from '~/integrations/react/mui';

export default component$(() => {

  return (
    <div class={"text-center"}>
    <div>
      <MuiForm className='w-1/2'>
      <MuiInputLabel htmlFor="my-input">Email address</MuiInputLabel>
      <MuiInput id="my-input" aria-describedby="my-helper-text" />
      <MuiInputLabel className='mt-2' htmlFor='my-input'>Password</MuiInputLabel>
      <MuiInput id='password' type='password' aria-describedby='password' />
      </MuiForm>
    </div>
    <div>
    </div>
    <MUIButton className='bg-blue-200 w-1/2 mt-5' color='primary' >Login</MUIButton>
    </div>
  );
});