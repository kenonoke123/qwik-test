import { component$ } from '@builder.io/qwik';
import { MUIButton, MuiForm, MuiInput, MuiInputLabel } from '~/integrations/react/mui';

export default component$(() => {

  return (
    <div class={"text-center"}>
    <div>
      <MuiForm>
      <MuiInputLabel htmlFor="my-input">First name</MuiInputLabel>
      <MuiInput id="first-name" aria-describedby="my-helper-text" />
      <MuiInputLabel className='mt-2' htmlFor="my-input">Last name</MuiInputLabel>
      <MuiInput id="last-name" aria-describedby="my-helper-text" />
      <MuiInputLabel className='mt-2' htmlFor="my-input">Email address</MuiInputLabel>
      <MuiInput id="email" aria-describedby="my-helper-text" />
      <MuiInputLabel className='mt-2' htmlFor='my-input'>Password</MuiInputLabel>
      <MuiInput id='password' type='password' aria-describedby='password' />
      <MuiInputLabel className='mt-2' htmlFor='my-input'>Repeat Password</MuiInputLabel>
      <MuiInput id='repeat-password' type='password' aria-describedby='repeat-password' />
      </MuiForm>
    </div>
    <div>
    </div>
    <MUIButton className='bg-blue-200 w-1/2 mt-5' color='primary' >Register</MUIButton>
    </div>
  );
});